import uuid

import psycopg2

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
read_from_inventory = "SELECT SUM(amount) FROM Inventory WHERE username = %s"
safe_update_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%s, %s, %s) ON CONFLICT (username, " \
                        "product) DO UPDATE SET amount = inventory.amount + EXCLUDED.amount "
write_transaction = "INSERT INTO Completed_transactions (transaction_id) VALUES (%s)"


def get_connection():
    return psycopg2.connect(
        dbname="lab7",
        user="postgres",
        password="postgres",
        host="127.0.0.1",
        port=5432
    )


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    transaction_id = str(uuid.uuid4())
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute("BEGIN")
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                cur.execute(read_from_inventory, (username,))
                inventory_amount = cur.fetchone()[0]
                if inventory_amount is not None:
                    if inventory_amount + amount > 100:
                        raise Exception("Inventory is full")

                cur.execute(safe_update_inventory, (username, product, amount))
                cur.execute(write_transaction, (transaction_id,))
                cur.execute("COMMIT")

            except psycopg2.errors.UniqueViolation:
                raise Exception("Transaction already completed")

            except Exception as e:
                conn.rollback()
                raise e


buy_product('Alice', 'marshmello', 1)
