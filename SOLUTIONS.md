# Lab7 - Rollbacks and reliability

## Lab

Lab queries:
![img.png](img.png)

## Homework

### Part 1

To keep database consistent developers use SQL transactions. Transaction are atomic meaning either they executed fully, or not executed at all e.g. no partial execution.
To make function consistent queries for `buy_decrease_balance` and `buy_decrease_stock` should be executed as transaction.

```python
try:
    cur.execute("BEGIN")
    cur.execute(buy_decrease_balance, obj)
    if cur.rowcount != 1:
        raise Exception("Wrong username")

    cur.execute(buy_decrease_stock, obj)
    if cur.rowcount != 1:
        raise Exception("Wrong product or out of stock")
    
    cur.execute("COMMIT")

except Exception as e:
    conn.rollback()
    raise e
```

### Part 2

Table for player inventory:
```postgresql
CREATE TABLE Inventory (
    username TEXT UNIQUE,
    product TEXT UNIQUE,
    amount INT CHECK(amount >= 0),
    CONSTRAINT inventory_pk PRIMARY KEY (username, product),
    CONSTRAINT inventory_limit CHECK (amount <= 100)
);
```

Queries for inventory:
```python
read_from_inventory = "SELECT SUM(amount) FROM Inventory WHERE username = %s"
safe_update_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%s, %s, %s) ON CONFLICT (username, " \
                   "product) DO UPDATE SET amount = inventory.amount + EXCLUDED.amount "
```

Changed code:
```python
def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute("BEGIN")
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                cur.execute(read_from_inventory, (username,))
                inventory_amount = cur.fetchone()[0]
                if inventory_amount is not None:
                    if inventory_amount + amount > 100:
                        raise Exception("Inventory is full")

                cur.execute(safe_update_inventory, (username, product, amount))
                cur.execute("COMMIT")

            except Exception as e:
                conn.rollback()
                raise e
```

## Extra 1

To not repeat same transaction after function failure we can create table of completed transactions with unique id. If user tries to execute transaction, unique transaction id will be generated and written to database, if we get `UniqueViolation` it means that transaction already completed.   

Transactions table:
```postgresql
CREATE TABLE Completed_transactions (transaction_id TEXT UNIQUE);
```

SQL query:
```python
write_transaction = "INSERT INTO Completed_transactions (transaction_id) VALUES (%s)"
```

Updated code:
```python
def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    transaction_id = str(uuid.uuid4())
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute("BEGIN")
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                cur.execute(read_from_inventory, (username,))
                inventory_amount = cur.fetchone()[0]
                if inventory_amount is not None:
                    if inventory_amount + amount > 100:
                        raise Exception("Inventory is full")

                cur.execute(safe_update_inventory, (username, product, amount))
                cur.execute(write_transaction, (transaction_id,))
                cur.execute("COMMIT")

            except psycopg2.errors.UniqueViolation:
                raise Exception("Transaction already completed")

            except Exception as e:
                conn.rollback()
                raise e
```
